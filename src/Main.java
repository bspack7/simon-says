import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Glow;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

//    Button button = new Button();
//    Glow glow = new Glow();
//
//    button.setEffect(glow);
//    button.setEffect(NULL);

    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/Untitled1.fxml"));
        Parent mainPane = loader.load();
        Controller mainController = (Controller)loader.getController();

        StackPane sPane = new StackPane();
        Text text = new Text("Text Here");
        sPane.getChildren().add(text);

//        mainController.setPaneCenter(sPane);
//        mainController.getButtonBarController().setBtn1Action(e -> {
//            text.setText("You Pressed Me");
//        });
//
//        mainController.getRandomBarController().printMessage();

        primaryStage.setTitle("");
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
