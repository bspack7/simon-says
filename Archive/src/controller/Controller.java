package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.effect.Glow;

import java.io.*;
import java.util.*;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;


public class Controller {
    @FXML
    private Button button1;

    @FXML
    private Button button2;

    @FXML
    private Button button3;

    @FXML
    private Button button4;

    @FXML
    private Button start;

    @FXML
    private MenuBar menubar;

    @FXML
    private ColorPicker cpicker;

    @FXML
    private GridPane gpane;

    @FXML
    Label label;

    @FXML
    Label current;

    Glow glow;
    MidiChannel midi;
    ArrayList<Button> simon;
    ArrayList<Button> buttons;
    ArrayList<Button> userPlay;
    ArrayList<Integer> highScores;
    ArrayList<Integer> scoresFromFile;
    int noteTmp = 0;
    int instrumentTmp = 0;
    int currentScore = 0;
    int gameSize = 4;
    boolean userTurn = false;
    boolean found = false;

    @FXML
    public void initialize()
    {
        glow = new Glow();
        simon = new ArrayList<Button>();
        buttons = new ArrayList<Button>();
        userPlay = new ArrayList<Button>();
        highScores = new ArrayList<Integer>();
        scoresFromFile = new ArrayList<Integer>();
        button1.setBackground(new Background(new BackgroundFill(Color.STEELBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        button2.setBackground(new Background(new BackgroundFill(Color.KHAKI, CornerRadii.EMPTY, Insets.EMPTY)));
        button3.setBackground(new Background(new BackgroundFill(Color.TOMATO, CornerRadii.EMPTY, Insets.EMPTY)));
        button4.setBackground(new Background(new BackgroundFill(Color.SEAGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        buttons.add(button1);
        buttons.add(button2);
        buttons.add(button3);
        buttons.add(button4);
        initMidi();
        loadScores();
        label.setText("" + highScores.get(0));
    }

    public void doGlow(MouseEvent event)
    {
        if (!start.isDisabled())
        {
            ((Button) event.getSource()).setBackground(new Background(new BackgroundFill(cpicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY)));
        }
        if (userTurn)
        {
            Button source = (Button) event.getSource();
//            gpane.getRowIndex(source);
//            gpane.getColumnIndex(source);
//            if (gpane.getColumnIndex(source) == 0 && gpane.getRowIndex(source) == 0)
//            {
//                noteTmp = 10;
//                instrumentTmp = 10;
//            }
//            else if (gpane.getColumnIndex(source) == 1 && gpane.getRowIndex(source) == 0)
//            {
//                noteTmp = 30;
//                instrumentTmp = 30;
//            }
//            else if (gpane.getColumnIndex(source) == 0 && gpane.getRowIndex(source) == 1)
//            {
//                noteTmp = 50;
//                instrumentTmp = 50;
//            }
//            else
//            {
//                noteTmp = 70;
//                instrumentTmp = 70;
//            }
            playNote();
            source.setEffect(glow);
            userPlay.add(source); // take the button they click, add it to "userPlay"
        }
    }

    private void messedUp()
    {
        String rank = "";
        highScore(currentScore);
        if (currentScore <=3)
        {
            rank = "Loser";
        }
        else if (currentScore > 3 && currentScore <= 10)
        {
            rank = "Baller";
        }
        else
        {
            rank = "God";
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("You done messed up!");
        alert.setHeaderText(null);
        alert.setContentText("Your Final Score was " + currentScore + ". You are a " + rank + "!");
        alert.show();
        userTurn = false;
        simon.clear();
        userPlay.clear();
        currentScore = 0;
        start.setDisable(false);
    }

    public void releaseGlow()
    {
        if (userTurn)
        {
            endNote();
            boolean rightAnswer = true;

            for (int i = 0; i < userPlay.size() && i < simon.size(); i++) // check to see if userPlay matches with simon's
            {
                if (userPlay.get(i) != simon.get(i)) // if not, stop the game
                {
                    messedUp();
                    rightAnswer = false;
                    break;
                }
            }
            if (userPlay.size() == simon.size() && rightAnswer) // give me the next challenge from Simon!
            {
                runGame();
                currentScore++;
                current.setText("" + currentScore);
            }
            button1.setEffect(null);
            button2.setEffect(null);
            button3.setEffect(null);
            button4.setEffect(null);
        }
    }

    public void start()
    {
        start.setDisable(true);
        if (simon != null) {
            simon.clear();
        }
        runGame();
        userTurn = false;
    }

    private void runGame()
    {
//        current.setText("" + currentScore);
        userPlay.clear();
        showPattern();
    }

    private void showPattern()
    {
        userTurn = false;
        Random rand = new Random();
        int randomButton = rand.nextInt(4);

        simon.add(buttons.get(randomButton));

        new Thread(() -> {

        for (int i = 0; i < simon.size(); i++)
        {
            int j = i;

            try {
                Thread.sleep(700);
            }
            catch (InterruptedException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }

            simon.get(i).setEffect(glow);
            playNote();

            try {
                Thread.sleep(700);
            }
            catch (InterruptedException ex) {
                // TODO Auto-generated catch block
                ex.printStackTrace();
            }
                simon.get(j).setEffect(null);
                endNote();
        }
        userTurn = true;
        }).start();
    }

    private void highScore(int score)
    {
        loadScores();
        if (highScores.size() < 10)
        {
            highScores.add(0, score);
                for (int i = 0; i < highScores.size(); i++)
                {
                    System.out.print(highScores.get(i) + " ");
                }
                System.out.print("\n");
        }
        else
        {
            for (int i = 0; i < highScores.size(); i++)
            {
                if (score > highScores.get(i))
                {
                highScores.remove(highScores.size() - 1);
                highScores.add(i, score);
                break;
                }
            }
            for (int i = 0; i < highScores.size(); i++)
            {
                System.out.print(highScores.get(i) + " ");
            }
            System.out.print("\n");
        }
        Collections.sort(highScores);
        Collections.reverse(highScores);
        printScores();
    }

    private void printScores() {
        label.setText("" + highScores.get(0));
        try
        {
        PrintWriter print = new PrintWriter(new FileOutputStream("src/data/score.txt"));
            for (int i = 0; i < highScores.size(); i++)
            {
                print.println(highScores.get(i));
            }
        print.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadScores()
    {
        highScores.clear();
        try {
            FileReader reader = new FileReader("src/data/score.txt");
            Scanner scan = new Scanner(reader);
            while (scan.hasNext())
            {
                highScores.add(Integer.parseInt(scan.next()));
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void playNote() {
        // TODO Auto-generated method stub
        try {
            noteTmp = 50;
            instrumentTmp = 50;
        } catch (NumberFormatException e1) {
            // TODO Auto-generated catch block
        }

        midi.programChange(instrumentTmp);
        midi.noteOn(noteTmp, 150);
    }

    private void endNote() {
        midi.allNotesOff();
    }

    private void initMidi() {
        try {
            Synthesizer synth = MidiSystem.getSynthesizer();
            synth.open();
            midi = synth.getChannels()[0];
        } catch (MidiUnavailableException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}

